import config from "../../config";
import base64 from 'base-64';
class ApiService {
  async getJSON(route, data) {

    return _getJSON(route, data);
  }

  async callUrl(route, data) {
    return await _callUrl(route, data);
  }



}

async function _getJSON(route, data) {
  let urlResponse = await _callUrl(route, data);
  if (urlResponse) {
    return JSON.parse(urlResponse._bodyInit);
  }
  else
    return null;
}

async function _callUrl(route, data) {
  let absoluteRoute = config.apiRoute + route;
  if (data) {
    absoluteRoute += "?";
    absoluteRoute += Object.keys(data).map(key => `${key}=${data[key]}`).join("&");
  }
  console.log(absoluteRoute);
  let username = 'sensor';
  let password = 'sensor';

  let headers = new Headers();
  headers.append('Authorization', 'Basic ' + base64.encode(username + ":" + password));
  let urlResponse = await fetch(absoluteRoute, {
    method: 'GET',
    headers: headers,
    //credentials: 'user:passwd'
  });

  console.log(urlResponse);

  if (urlResponse.status === 200) {
    return urlResponse;
  } else {
    console.error("bad response");
    console.log(`route = ${route}`);
    console.log(`route for execution = ${absoluteRoute}`);
    return null;
  }
}

export let apiService = new ApiService();
