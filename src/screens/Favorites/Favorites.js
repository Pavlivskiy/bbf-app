import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View,
    ActivityIndicator,
    TouchableOpacity,
    Image,
    FlatList,
    Alert
} from 'react-native';
import { Button,Container,Header,Left,Right,Icon } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { showMessage, hideMessage } from "react-native-flash-message";
import DropdownAlert from 'react-native-dropdownalert';

const dataToSave = [
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain1',
        valueSensor: 4.3,

    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain2',
        valueSensor: 355.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain3',
        valueSensor: 6.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain4',
        valueSensor: 7.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain5',
        valueSensor: 354.7,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain6',
        valueSensor: 356.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain7',
        valueSensor: 357.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain8',
        valueSensor: 4.7,
        
    }
];
export default class TopFrop extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      userLogined: '',
      topCheck: true,
      flopCheck: false,
      criticalValue: 25,
      dataSensor: dataToSave,
    }
  }

  componentDidMount() {
    for(var i = 0, el = dataToSave.length; i < el; i++) {
        if(this.state.criticalValue <= dataToSave[i].valueSensor) {
            this.dropdown.alertWithType('warn', 'Threshold value is exceeded', 'If a threshold value is exceeded, the user is informed accordingly by a message/warning');
            break;
        }
    }
  }

  changeItem(data) {
    if(data === 'Flor') {
        this.setState({
            flopCheck: true,
            topCheck: false
        })
    } else if(data === "Top") {
        this.setState({
            flopCheck: false,
            topCheck: true
        })
    }
  }

  deleteSensor(item) {
    Alert.alert(
        `Delete ${item.title}`,
        'Are you sure you want to delete that location?',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: {color: 'gray'},
          },
          {text: 'Yes', onPress: () => this.Delete(item)},
        ],
        {cancelable: false},
      );
  }

  Delete(item) {
    console.log("Delete item");
    console.log(item);
    
    for(var i = 0, el = dataToSave.length; i < el; i++) {
        console.log(dataToSave[i].title);
        console.log('item: ' + item.title);
        if(item.title === dataToSave[i].title) {
            dataToSave.splice(i, 1);
            break;
        }
    }
    this.setState({
        dataSensor: dataToSave
    })
  }

  render() {
    return (
      <Container>
        <View styles={styles.container}>
          <Header style={styles.containerHeader}>
              <Left style={[{ flexDirection: 'row'}]}>
                <Icon onPress={() => this.props.navigation.navigate('Menu', {screenName: 'Favorites'})} name="ios-arrow-round-back" style={{ color: 'white', marginLeft: '10%',fontSize: 40 }} />
                {/* <Text style={{color: 'white', fontSize: 23, marginTop: 5}}> Favorites</Text> */}
              </Left>
              <Right>
              </Right>
          </Header>
          <DropdownAlert ref={ref => this.dropdown = ref} />
          <View style={{paddingLeft: '5%', paddingRight: '5%'}}>
            <View style={{width: '100%', textAlign: 'left', marginTop: '5%'}}>
              <Text style={{fontSize: 35, color: 'black', fontWeight: 'bold'}}>Favorites</Text>
            </View>
            <View style={{marginTop: '5%', width: '100%', height: '82%'}}>
                <FlatList
                    data={this.state.dataSensor}
                    renderItem={({item}) => 
                    <View style={{flexDirection: 'row', justifyContent: 'center', width: '100%', borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: '10%', paddingBottom: '5%'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Map')} style={{width: '15%', }}>
                            <Icon name="md-locate" style={{ color: 'gray', fontSize: 30, marginLeft: '5%' }} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Map')} style={{width: '55%'}}>
                            <Text style={{color: 'black'}}>{item.title}</Text>
                        </TouchableOpacity>
                        <View style={{width: '15%'}}>
                            {
                                this.state.criticalValue <= item.valueSensor ? 
                                <Icon name="ios-notifications" style={{ color: 'red', fontSize: 30 }} /> : null
                            }
                        </View>
                        <TouchableOpacity onPress={() => this.deleteSensor(item)} style={{width: '15%'}}>
                            <Icon name="ios-trash" style={{ color: 'gray', fontSize: 30 }} />
                        </TouchableOpacity>
                    </View>}
                />
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: '5%'
  },
  containerHeader: {
    backgroundColor: '#6ec1e3',
    // borderColor: 'transparent',
    // borderBottomWidth: 0,
    // shadowColor: 'white'
  },
});
