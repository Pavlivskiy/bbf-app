import React, {Component} from 'react';
import {Platform, StyleSheet, View, TouchableOpacity, Switch, ScrollView, Dimensions} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import * as data from '../../languages/en.json';
import { CheckBox } from 'react-native-elements'
import { Button,Container,Header,Left,Right,Icon,Text } from 'native-base';

export default class App extends Component {
    static navigationOptions = { title: data.map, header: { visible:false } };
    constructor(props) {
      super(props);
      this.state = {
        checkAQI: true,
        checkPM10: true,
        checkPM25: true,
        checkNO2: true,
        checkCO: true,
        checkSO2: true,
        checkO3: true,
        langCheck: null
      }
      this.saveData = this.saveData.bind(this);
    } 

    saveData () {
      var data = {
        AQI: this.state.checkAQI,
        PM10: this.state.checkPM10,
        PM25: this.state.checkPM25,
        NO2: this.state.checkNO2,
        CO: this.state.checkCO,
        SO2: this.state.checkSO2,
        O3: this.state.checkO3,
      }
      let dayaToString = JSON.stringify(data);
      try {
        AsyncStorage.setItem('settings', dayaToString).then((res) => {
          console.log('res');
          console.log(res)
          this.props.navigation.navigate('Menu', {screenName: 'Settings'})
        });
       
      } catch (error) {
        console.log(error);
      }
      
    }
    componentDidMount = async () => {
      const value = await AsyncStorage.getItem('settings');
      if (value !== null) {
        let data = JSON.parse(value);
        console.log(data);
        this.setState({
          checkAQI: data.AQI,
          checkPM10: data.PM10,
          checkPM25: data.PM25,
          checkNO2: data.NO2,
          checkCO: data.CO,
          checkSO2: data.SO2,
          checkO3: data.O3
        });
      }
    }
    render() {
        return (
          <Container>
            <Header style={styles.containerHeader}>
              <Left style={{ flexDirection: 'row'}}>
                <Icon size={30} onPress={() => this.saveData()} name="ios-arrow-round-back" style={{ color: 'white', marginLeft: '10%', fontSize: 40}} />
              </Left>
              <Right>
              </Right>
            </Header>
        <View style={styles.container}>
          
          <View style={{padding: '7%', width: '100%', height: '100%'}}>
            
            <View style={{width: '100%', textAlign: 'left'}}>
              <Text style={{fontSize: 35}}>{data.Settings.display} </Text>
            </View>
            <View style={{height: '5%'}}></View>
            <ScrollView>
              <View style={{height: '100%'}}>
                <View style={{flexDirection: 'row', padding: 0}}>
                  <CheckBox
                    checkedIcon='check-circle-o'
                    uncheckedIcon='circle-o'
                    checked={true}
                    textStyle={{backgroundColor:'#6ec1e3', }}
                    checkedColor={'grey'}
                    containerStyle={{backgroundColor:'transparent', borderColor: 'transparent', padding: 0, position: 'relative', marginLeft: 0}}
                    // onPress={() => this.setState({checkAQI: !this.state.checkAQI})}
                  />  
                  <View style={{marginTop: 5, marginLeft: 10}}>
                    <Text style={{color: 'grey', fontSize: 20, fontFamily: 'Avenir-Book'}}>AQI</Text>
                  </View>
                </View>
                <View style={{height: 30}}></View>
                <View style={{flexDirection: 'row', padding: 0}}>
                  <CheckBox
                    checkedIcon='check-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checkPM10}
                    textStyle={{backgroundColor:'#6ec1e3', }}
                    checkedColor={'#6ec1e3'}
                    containerStyle={{backgroundColor:'transparent', borderColor: 'transparent', padding: 0, position: 'relative', marginLeft: 0}}
                    onPress={() => this.setState({checkPM10: !this.state.checkPM10})}
                  />  
                  <View style={{marginTop: 5, marginLeft: 10}}>
                    <Text style={{fontSize: 20}}>PM<Text style={{fontSize: 10}}>10</Text></Text>
                  </View>
                </View>
                <View style={{height: 30}}></View>
                <View style={{flexDirection: 'row', padding: 0}}>
                  <CheckBox
                    checkedIcon='check-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checkPM25}
                    textStyle={{backgroundColor:'#6ec1e3', }}
                    checkedColor={'#6ec1e3'}
                    containerStyle={{backgroundColor:'transparent', borderColor: 'transparent', padding: 0, position: 'relative', marginLeft: 0}}
                    onPress={() => this.setState({checkPM25: !this.state.checkPM25})}
                  />  
                  <View style={{marginTop: 5, marginLeft: 10}}>
                    <Text style={{fontSize: 20, fontFamily: 'Avenir-Book'}}>PM<Text style={{fontSize: 10, fontFamily: 'Avenir-Book'}}>2.5</Text></Text>
                  </View>
                </View>
                <View style={{height: 30}}></View>
                <View style={{flexDirection: 'row', padding: 0}}>
                  <CheckBox
                    checkedIcon='check-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checkNO2}
                    textStyle={{backgroundColor:'#6ec1e3', }}
                    checkedColor={'#6ec1e3'}
                    containerStyle={{backgroundColor:'transparent', borderColor: 'transparent', padding: 0, position: 'relative', marginLeft: 0}}
                    onPress={() => this.setState({checkNO2: !this.state.checkNO2})}
                  />  
                  <View style={{marginTop: 5, marginLeft: 10}}>
                    <Text style={{fontSize: 20, fontFamily: 'Avenir-Book'}}>NO<Text style={{fontSize: 10, fontFamily: 'Avenir-Book'}}>2</Text></Text>
                  </View>
                </View>
                <View style={{height: 30}}></View>
                <View style={{flexDirection: 'row', padding: 0}}>
                  <CheckBox
                    checkedIcon='check-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checkCO}
                    textStyle={{backgroundColor:'#6ec1e3', }}
                    checkedColor={'#6ec1e3'}
                    containerStyle={{backgroundColor:'transparent', borderColor: 'transparent', padding: 0, position: 'relative', marginLeft: 0}}
                    onPress={() => this.setState({checkCO: !this.state.checkCO})}
                  />  
                  <View style={{marginTop: 5, marginLeft: 10}}>
                    <Text style={{fontSize: 20, fontFamily: 'Avenir-Book'}}>CO<Text style={{fontSize: 10, fontFamily: 'Avenir-Book'}}></Text></Text>
                  </View>
                </View>
                <View style={{height: 30}}></View>
                <View style={{flexDirection: 'row', padding: 0}}>
                  <CheckBox
                    checkedIcon='check-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checkSO2}
                    textStyle={{backgroundColor:'#6ec1e3', }}
                    checkedColor={'#6ec1e3'}
                    containerStyle={{backgroundColor:'transparent', borderColor: 'transparent', padding: 0, position: 'relative', marginLeft: 0}}
                    onPress={() => this.setState({checkSO2: !this.state.checkSO2})}
                  />  
                  <View style={{marginTop: 5, marginLeft: 10}}>
                    <Text style={{fontSize: 20, fontFamily: 'Avenir-Book'}}>SO<Text style={{fontSize: 10, fontFamily: 'Avenir-Book'}}>2</Text></Text>
                  </View>
                </View>
                <View style={{height: 30}}></View>
                <View style={{flexDirection: 'row', padding: 0}}>
                  <CheckBox
                    checkedIcon='check-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checkO3}
                    textStyle={{backgroundColor:'#6ec1e3', }}
                    checkedColor={'#6ec1e3'}
                    containerStyle={{backgroundColor:'transparent', borderColor: 'transparent', padding: 0, position: 'relative', marginLeft: 0}}
                    onPress={() => this.setState({checkO3: !this.state.checkO3})}
                  />  
                  <View style={{marginTop: 5, marginLeft: 10}}>
                    <Text style={{fontSize: 20, fontFamily: 'Avenir-Book'}}>O<Text style={{fontSize: 10, fontFamily: 'Avenir-Book'}}>3</Text></Text>
                  </View>
                </View>
              </View>
            
            </ScrollView>
          </View>
        </View>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    height:'100%', width:'100%'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'left',
    margin: 10,
    color: 'silver'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  widthFull: {
    width: '90%',
  },
  sendButton: {
    height: 50,
    width: '100%',
    backgroundColor: 'blue',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 15
  },
  checkBox: {
    color: 'blue'
  },
  containerHeader: {
    backgroundColor: '#6ec1e3',
  }
});
