import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View,
    ActivityIndicator,
    TouchableOpacity,
    Image,
    FlatList
} from 'react-native';
import { Button,Container,Header,Left,Right,Icon } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

const dataToSave = [
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.3,

    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 355.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 356.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 357.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.7,
        
    }
];
const dataToSaveFlop = [
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.1,

    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 355.2,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 356.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 357.4,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.5,
        
    }
];
const data = [
    "hello",
    'newOne'
]

export default class TopFrop extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      userLogined: '',
      topCheck: true,
      flopCheck: false
    }
  }

  componentDidMount() {
    //this.getData();

  }

  changeItem(data) {
    if(data === 'Flor') {
        this.setState({
            flopCheck: true,
            topCheck: false
        })
    } else if(data === "Top") {
        this.setState({
            flopCheck: false,
            topCheck: true
        })
    }
  }

  render() {
    return (
      <Container>
        <View styles={styles.container}>
          <Header style={styles.containerHeader}>
              <Left style={[{ flexDirection: 'row'}]}>
                <Icon onPress={() => this.props.navigation.navigate('Menu', {screenName: 'Top'})} name="ios-arrow-round-back" style={{ color: 'white', marginLeft: '10%',fontSize: 40 }} />
                <Text style={{color: 'white', fontSize: 23}}> Top/Flop</Text>
              </Left>
              <Right>
              </Right>
              
          </Header>
          <View style={{backgroundColor: '#6ec1e3', flexDirection: 'row', height: '11%', justifyContent: 'center'}}>
              <View style={{width: '49%', alignItems: 'center', justifyContent: 'center'}}>
              <TouchableOpacity onPress={() => this.changeItem('Top')}>
                    <Text style={[{fontSize: 18}, this.state.topCheck ? {color: 'white'} : {color: 'grey'}]}>TOP</Text>
                </TouchableOpacity>
              </View>
              <View style={{height: '50%', borderWidth: 1, borderColor: 'white', marginTop: '4%'}}>

              </View>
              <View style={{width: '49%', alignItems: 'center', justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => this.changeItem('Flor')}>
                    <Text style={[{fontSize: 18}, this.state.flopCheck ? {color: 'white'} : {color: 'grey'}]}>Flop</Text>
                </TouchableOpacity>
              </View>
          </View>
          {/* <View style={{paddingLeft: '10%', paddingRight: '10%', marginTop: '5%', height: '12%'}}>
            <View style={{backgroundColor: '#6ec1e3', flexDirection: 'row', justifyContent: 'center', width: '100%', height: '100%', borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                <View style={{width: '40%', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: 'white', fontSize: 18}}>Location</Text>
                </View>
                <View style={{width: '30%', alignItems: 'center', justifyContent: 'center'}}>
                    
                </View>
                <View style={{width: '30%', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color: 'white', fontSize: 18}}>PM</Text><Text style={{color: 'white', fontSize: 13, marginTop: 5}}>10</Text>
                    </View>
                </View>
            </View>
          </View> */}
          {
              this.state.topCheck ? 
              <View style={{paddingLeft: '10%', paddingRight: '10%', height: '100%'}}>
                <FlatList
                    data={dataToSave}
                    renderItem={({item}) => 
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Map')} style={{flexDirection: 'row', justifyContent: 'center', width: '100%', borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: '10%', paddingBottom: '5%'}}>
                        <View style={{width: '18%'}}>
                            <Icon name="md-locate" style={{ color: 'gray', marginLeft: '10%',fontSize: 40 }} />
                        </View>
                        <View style={{width: '60%'}}>
                            <Text style={{color: 'black'}}>{item.title}</Text>
                        </View>
                        <View style={{width: '22%', flexDirection: 'row'}}>
                            <Text style={{color: 'black'}}>{item.valueSensor} PM</Text><Text style={{fontSize: 10, color: 'black', marginTop: 5}}>10</Text>
                        </View>
                    </TouchableOpacity>}
                />
              
            
          </View> : null
          }

{
              this.state.flopCheck ? 
              <View style={{paddingLeft: '10%', paddingRight: '10%', height: '100%'}}>
                <FlatList
                    data={dataToSaveFlop}
                    renderItem={({item}) => 
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Map')} style={{flexDirection: 'row', justifyContent: 'center', width: '100%', borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: '10%', paddingBottom: '5%'}}>
                        <View style={{width: '18%'}}>
                            <Icon name="md-locate" style={{ color: 'gray', marginLeft: '10%',fontSize: 40 }} />
                        </View>
                        <View style={{width: '60%'}}>
                            <Text style={{color: 'black'}}>{item.title}</Text>
                        </View>
                        <View style={{width: '22%'}}>
                        <Text style={{color: 'black'}}>{item.valueSensor} PM</Text><Text style={{fontSize: 10, color: 'black', marginTop: 5}}>10</Text>
                        </View>
                    </TouchableOpacity>}
                />
              
            
          </View> : null
          }
          
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: '5%'
  },
  containerHeader: {
    backgroundColor: '#6ec1e3',
    // borderColor: 'transparent',
    // borderBottomWidth: 0,
    // shadowColor: 'white'
  },
});
