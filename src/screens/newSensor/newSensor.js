import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View,
    ActivityIndicator,
    TouchableOpacity,
    Image,
    FlatList,
    Alert
} from 'react-native';
import { Button,Container,Header,Left,Right,Icon } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

const dataToSave = [
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.3,

    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 355.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 356.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 357.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.7,
        
    },
    {
      title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
      valueSensor: 356.3,
      
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 357.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.7,
        
    }
];
export default class TopFrop extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      userLogined: '',
      topCheck: true,
      flopCheck: false
    }
  }

  getDataSensors =  async () => {
    try {
      const value = await AsyncStorage.getItem('ownSensors');
      console.log('user login');
      console.log(value);
      console.log(typeof value);

      if(value === "null") {
        
        this.setState({
          userLogined: null
        })
      } else {
        this.setState({
          userLogined: value
        })
      }
    } catch(e) {
      console.log(e);
    }
  }

  deleteSensor(item) {
    Alert.alert(
        `Delete ${item.title}`,
        'Are you sure you want to delete that location?',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: {color: 'gray'},
          },
          {text: 'Yes', onPress: () => this.Delete(item)},
        ],
        {cancelable: false},
      );
  }

  Delete(item) {
    console.log("Delete item");
    console.log(item);
    
    for(var i = 0, el = dataToSave.length; i < el; i++) {
        console.log(dataToSave[i].title);
        console.log('item: ' + item.title);
        if(item.title === dataToSave[i].title) {
            dataToSave.splice(i, 1);
            break;
        }
    }
    this.setState({
        dataSensor: dataToSave
    })
  }

  componentDidMount() {
    this.getDataSensors();
  }

  changeItem(data) {
    if(data === 'Flor') {
        this.setState({
            flopCheck: true,
            topCheck: false
        })
    } else if(data === "Top") {
        this.setState({
            flopCheck: false,
            topCheck: true
        })
    }
  }

  renderOwnSensors() {
    return(
      <View style={{height: '90%', padding: '5%'}}>
        <View>
          <Text style={{fontSize: 35, color: 'black'}}>Own sensors</Text>
        </View>
        <View>
          <FlatList
                    data={dataToSave}
                    scrollEnabled={true}
                    renderItem={(item) => 
                    <View style={{flexDirection: 'row', justifyContent: 'center', width: '100%', borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: '10%', paddingBottom: '5%'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Map')} style={{width: '55%'}}>
                            <Text style={{color: 'black'}}>{item.item.title}</Text>
                        </TouchableOpacity>
                        {console.log(item.item.title)}
                        <View style={{width: '15%'}}>
                            {
                                this.state.criticalValue <= item.valueSensor ? 
                                <Icon name="ios-notifications" style={{ color: 'red', fontSize: 30 }} /> : null
                            }
                        </View>
                        <TouchableOpacity onPress={() => this.deleteSensor(item.item)} style={{width: '15%', alignItems: 'flex-end'}}>
                            <Icon name="ios-trash" style={{ color: 'gray', fontSize: 30 }} />
                        </TouchableOpacity>
                    </View>}
                />
        </View>
      </View>
    );
  }

  renderSearchSensors() {
    return(
      <View>
        
      </View>
    );
  }

  render() {
    return (
      <Container>
        <View styles={styles.container}>
          <Header style={styles.containerHeader}>
              <Left style={[{ flexDirection: 'row'}]}>
                <Icon onPress={() => this.props.navigation.navigate('Menu', {screenName: 'OwnSensors'})} name="ios-arrow-round-back" style={{ color: 'white', marginLeft: '10%',fontSize: 40 }} />
              </Left>
              <Right>
                <Icon name="ios-add-circle-outline" style={{ color: 'white', marginLeft: '10%',fontSize: 25 }} />
              </Right>
              
          </Header>
          
          {this.renderOwnSensors()}

        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: '5%'
  },
  containerHeader: {
    backgroundColor: '#6ec1e3',
    // borderColor: 'transparent',
    // borderBottomWidth: 0,
    // shadowColor: 'white'
  },
});
