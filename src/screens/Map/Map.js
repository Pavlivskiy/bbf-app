import React, {Component} from 'react';
import {Platform, StyleSheet, View, Dimensions} from 'react-native';
import * as data from '../../languages/en.json';
import { Button,Container,Header,Left,Right,Icon,Text } from 'native-base';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import AsyncStorage from '@react-native-community/async-storage';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMapReady: false,
      region: {
        latitude: 48.137154,
         longitude: 11.576124,
        latitudeDelta: 40.0000467769713,
        longitudeDelta: 40.0421,
      }
    }
  }

  storeData = async () => {
    try {
      await AsyncStorage.setItem('dataMenuItem', 'Map')
    } catch (e) {
      console.log('errrrrooooorrrrr');
      console.log(e);
    }
  }

  componentDidMount() {
    this.storeData();
  }

  render() {
        return (
          <View style={styles.container}>
          < MapView
        style = {styles.map}
        initialRegion = {{
          latitude: 48.137154,
       longitude: 11.576124,
      latitudeDelta: 40.0000467769713,
      longitudeDelta: 40.0421,
       }}
     />
     <View style={{position: 'absolute', top: '5%', width: '80%', backgroundColor: 'white', flexDirection: 'row', height: '5%', borderRadius: 5}}>
              <Left style={{ flexDirection: 'row', marginLeft: '5%'}}>
                <Icon onPress={() => this.props.navigation.navigate("Menu")} name="md-menu" style={{ color: 'black', marginRight: 15 }} />
              </Left>
              <Right>
              </Right>
            </View>
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  containerHeader: {
    backgroundColor: 'transparent',
  },
  map: {
    flex: 1,
    height: height,
    width: width
  }
});
