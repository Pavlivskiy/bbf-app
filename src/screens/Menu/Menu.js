import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View,
    ActivityIndicator,
    TouchableOpacity,
    Image
} from 'react-native';
import { Button,Container,Header,Left,Right,Icon } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import apiService from '../../api/apiService';
export default class Congratulation extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      userLogined: ''
    }
  }

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('UserLogin');
      console.log('user login');
      console.log(value);
      console.log(typeof value);

      if(value === "null") {
        
        this.setState({
          userLogined: null
        })
      } else {
        this.setState({
          userLogined: value
        })
      }
    } catch(e) {
      console.log(e);
    }
  }

  componentDidMount() {
    this.getData();
  }

  logOut = async () => {
    try {
      await AsyncStorage.setItem('UserLogin', "null");
      this.props.navigation.navigate("Map");
    } catch (e) {
      console.log(e);
    }
  }

  renderLogin() {
    const { navigation } = this.props;  
    const user_name = navigation.getParam('screenName', 'Map'); 
      return(
          <View style={{position: 'absolute', top: 100 }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("Map")} style={styles.fontPadding}><Text style={[styles.fontItem, user_name === "Map" ? styles.colorBlue : null]}>Map</Text></TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("Favorites")} style={styles.fontPadding}><Text style={[styles.fontItem, user_name === "Favorites" ? styles.colorBlue : null]}>Favorites</Text></TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("Top")} style={styles.fontPadding}><Text style={[styles.fontItem, user_name === "Top" ? styles.colorBlue : null]}>Top/Flop</Text></TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("Settings")} style={styles.fontPadding}><Text style={[styles.fontItem, user_name === "Settings" ? styles.colorBlue : null]}>Settings</Text></TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("OwnSensors")} style={styles.fontPadding}><Text style={[styles.fontItem, user_name === "OwnSensors" ? styles.colorBlue : null]}>Own sensors</Text></TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("FAQ")} style={styles.fontPadding}><Text style={[styles.fontItem, user_name === "FAQ" ? styles.colorBlue : null]}>FAQ</Text></TouchableOpacity>
              <TouchableOpacity onPress={() => this.logOut()} style={styles.fontPadding}><Text style={[styles.fontItem]}>Log out</Text></TouchableOpacity>
          </View>
      );
  }

  renderNotLogin() {
    return(
        <View style={{position: 'absolute', top: 100 }}>
          <TouchableOpacity style={styles.fontPadding} onPress={() => this.props.navigation.navigate("Login")}><Text style={[styles.fontItem]}>Login</Text></TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate("Map")} style={styles.fontPadding}><Text style={[styles.fontItem]}>Map</Text></TouchableOpacity>
          <TouchableOpacity style={styles.fontPadding} disabled={true}><Text style={[styles.fontItem, {color: 'grey'}]}>Favorites</Text></TouchableOpacity>
          <TouchableOpacity style={styles.fontPadding} disabled={true}><Text style={[styles.fontItem, {color: 'grey'}]}>Top/Flop</Text></TouchableOpacity>
          <TouchableOpacity style={styles.fontPadding} disabled={true}><Text style={[styles.fontItem, {color: 'grey'}]}>Settings</Text></TouchableOpacity>
          <TouchableOpacity style={styles.fontPadding} disabled={true}><Text style={[styles.fontItem, {color: 'grey'}]}>Own sensors</Text></TouchableOpacity>
          <TouchableOpacity style={styles.fontPadding}><Text style={[styles.fontItem]}>FAQ</Text></TouchableOpacity>
        </View>
    );
  }

  render() {
    const { navigation } = this.props;  
    const user_name = navigation.getParam('screenName', 'Map'); 
    return (
      <Container>
        <View styles={styles.container}>
          <Icon size={30} onPress={() => this.props.navigation.navigate(user_name)} name="md-close" style={{ color: 'black', marginLeft: 20,
          marginTop: 20, fontSize: 40}} />
          { this.state.userLogined != null ? this.renderLogin() : this.renderNotLogin() }
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: '5%'
  },
  fontItem: {
    fontSize: 35,
    color: 'black'
  },
  fontPadding: {
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  colorBlue: {
    color: '#6ec1e3'
  },
  containerHeader: {
    backgroundColor: 'transparent',
    borderBottomWidth: 0,
    shadowRadius: 0,
    shadowOpacity: 0,
},
});
