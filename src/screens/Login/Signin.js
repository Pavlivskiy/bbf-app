import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View,
    ActivityIndicator,
    TouchableOpacity,
    Image
} from 'react-native';
import * as data from '../../languages/en.json';
import Reinput from 'reinput';
import PasswordInputText from 'react-native-hide-show-password-input';
import { Item, Input, Icon, Label, Button, Container,Header,Left,Right  } from 'native-base';
import { HelperText, TextInput } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';

export default class Signin extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      checkUserAccount: false,
      invalidEmail: false,
      invalidPass: false,
      userEmail: "",
      userPass: '',
      confPass: '',
      diffPass: false,
      icon: "eye-off",
      password: true,
      text: '',
      showPass: true
    }
    this.checkPass = this.checkPass.bind(this);
    this.emailSend = this.emailSend.bind(this);
  }

  storeData = async (data) => {
    try {
      await AsyncStorage.setItem('UserLogin', data)
    } catch (e) {
      console.log('errrrrooooorrrrr');
      console.log(e);
    }
  }

  componentDidMount () {
    //console.log(data.Login.UserName);

  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  emailSend(text) {
    console.log(text);
    if(this.validateEmail(text)) {
      this.setState({
        invalidEmail: false,
        userEmail: text
      });
    } else {
      this.setState({
        invalidEmail: true,
        userEmail: text
      });
    }
  }

  checkPass(text) {
    if(text.length < 8) {
      this.setState({
        invalidPass: true,
        userPass: text
      });
    } else {
      this.setState({
        invalidPass: false,
        userPass: text
      });
    }
  }

  validationForm = async () => {
    let { state } = this.state;
    if(this.state.userPass === "" || this.state.userPass === " ") {
      this.setState({
        invalidPass: true
      });
    } else if(this.state.userEmail === "" || this.state.userEmail === " ") {
      this.setState({
        invalidEmail: true
      });
    } else {
      let usersData = {
        userEmail: this.state.userEmail,
        userPass: this.state.userPass
      }
      console.log('userdata');
      console.log(usersData);

      let usersDataString = usersData.toString();

      this.storeData(JSON.stringify(usersData));

      this.props.screenProps.rootNavigation.navigate('Map');
      console.log(usersDataString);
      // try {
      //   await AsyncStorage.setItem('UsersData', JSON.stringify(usersData));
      // } catch (error) {
      //   // Error saving data
      //   console.log(error);
      // }
      
      // this.setState({
      //   checkUserAccount: true
      // });
    }

  }

  _changeIcon() {
    console.log(this.state.showPass);
    this.setState(prevState => ({
        icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
        showPass: !prevState.showPass
    }));
}

  renderForm() {
    return (
      <Container>
            <Header style={styles.containerHeader}>
              <Left style={[{ flexDirection: 'row'}]}>
                <Icon onPress={() => this.props.navigation.navigate('Home')} name="ios-arrow-round-back" style={{ color: 'black', marginLeft: '10%',fontSize: 40 }} />
              </Left>
              <Right>
              </Right>
            </Header>
      <KeyboardAwareScrollView style={styles.container} behavior="padding" enabled>
        <View style={{marginTop: '20%'}}>
          <View style={{width: '100%', textAlign: 'left'}}>
            <Text style={{fontSize: 35, fontWeight: 'bold'}}>{data.Login.Login} </Text>
          </View>
          <View style={{marginTop: '10%'}}>
            <TextInput
              label={data.Login.email}
              value={this.state.userEmail}
              style={{backgroundColor: 'white'}}
              error={this.state.invalidEmail}
              onChangeText={(text) => this.emailSend(text)}
            />
          </View>
          <View style={{flexDirection: 'row', marginTop: '10%'}}>
            <TextInput
              label={data.Login.Password}
              value={this.state.userPass}
              style={{backgroundColor: 'white', width: '100%'}}
              secureTextEntry={this.state.showPass}
              error={this.state.invalidPass}
              onChangeText={(text) => this.checkPass(text)}
            />
              <Icon style={{position: 'absolute', top: 20, right: 10}} name={this.state.icon} onPress={() => this._changeIcon()} />
          </View>
          {this.state.invalidPass ? <Text style={styles.emailInvalid}>{data.Login.invalidPassword}</Text> : null}
          <View style={styles.widthFull}>
            <TouchableOpacity onPress={this.validationForm.bind(this)} style={styles.sendButton}><Text style={styles.colorWhite}>{data.Login.Login}</Text></TouchableOpacity>
          </View>
          <View style={styles.widthFullLess}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')} >
              <Text style={{textDecorationLine: 'underline'}}>Forgot your password?</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
      </Container>
    );
  }

  renderActivityInd() {
    return (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#00ff00" />
      </View>
    );
  }

  render() {
    return (
      !this.state.checkUserAccount ? 
        this.renderForm() : 
        this.renderActivityInd()

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: '5%'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'silver'
  },
  textInputField: {
    width: '100%',
    fontSize: 13,
    height: 30,
    // borderColor: 'silver', 
    // borderWidth: 2
    backgroundColor: 'white',
  },
  widthContainer: {
    width: '100%',
    alignItems: 'flex-start',
    textAlign: 'left',
  },
  flexDecoretion: {
    flexDirection: 'row'
  },
  importantField: {
    marginLeft: 10,
    color: 'red',
    fontSize: 30
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  sendButton: {
    height: 50,
    width: '100%',
    backgroundColor: '#6ec1e3',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 5
  },
  emailInvalid: {
    color: 'red',
    fontSize: 13
  },
  invalidForm: {
    borderColor: 'red'
  },
  mandatory: {
    width: '100%',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  colorRed: {
    color: 'red'
  },
  widthFull: {
    width: '100%',
    marginTop: 50
  },
  widthFullLess: {
    width: '100%',
    marginTop: 30
  },
  colorWhite: {
    color: 'white',
    fontSize: 20
  },
  textDecoration: {
    textDecorationLine: "underline",
    textDecorationStyle: "solid",
    textDecorationColor: "#000"
  },
  pass: {
    width: 50,
    height: 50,
    backgroundColor: 'blue'
  },
  containerHeader: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderBottomWidth: 0,
},
});
