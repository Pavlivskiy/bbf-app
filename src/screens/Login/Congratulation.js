import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View,
    ActivityIndicator,
    TouchableOpacity,
    AsyncStorage,
    Image
} from 'react-native';
import * as data from '../../languages/en.json';
import Reinput from 'reinput';
import PasswordInputText from 'react-native-hide-show-password-input';
// import { Item, Input, Icon, Label } from 'native-base';
import { Button,Container,Header,Left,Right,Icon } from 'native-base';
import { HelperText, TextInput } from 'react-native-paper';

export default class Congratulation extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {

    }
  }
  render() {
    return (
      <Container>
            <Header style={styles.containerHeader}>
              <Left style={[{ flexDirection: 'row'}]}>
                <Icon onPress={() => this.props.navigation.navigate('Home')} name="ios-arrow-round-back" style={{ color: 'black', marginLeft: '10%',fontSize: 40 }} />
              </Left>
              <Right>
                {/* <Icon name="md-search" style={{ color: 'white' }} /> */}
              </Right>
            </Header>
      <View styles={styles.container}>
          <View style={{marginTop: '20%'}}>
            <View style={{width: '100%', textAlign: 'left', padding: '5%'}}>
                <Text style={{fontSize: 35, fontWeight: 'bold'}}>{data.Login.Congratulation}! </Text>
                <Text style={{color: '#c6c6c6', marginTop: '7%', fontSize: 20}}>You have successfully registered.</Text>
                <Text style={{color: '#c6c6c6', marginTop: '7%', fontSize: 20}}>Plese follow the link sent</Text>
                <Text style={{color: '#c6c6c6', fontSize: 20}}>to your email to confirm registration.</Text>
            </View>
          </View>
          <View style={{marginTop: '20%'}}>
            <View style={styles.widthFull}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Signin')} style={styles.sendButton}><Text style={styles.colorWhite}>{data.Login.goToLogin}</Text></TouchableOpacity>
            </View>
          </View>
      </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: '5%'
  },
  widthFull: {
    width: '100%',
    padding: '5%'
  },
  sendButton: {
    height: 50,
    width: '100%',
    backgroundColor: '#6ec1e3',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 5
  },
  colorWhite: {
      color: 'white',
      fontSize: 20
  },
  containerHeader: {
    backgroundColor: 'transparent',
    borderBottomWidth: 0,
},
});
