import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View,
    ActivityIndicator,
    TouchableOpacity,
    AsyncStorage,
    Image,
    KeyboardAvoidingView
} from 'react-native';
import * as data from '../../languages/en.json';
import Reinput from 'reinput';
import PasswordInputText from 'react-native-hide-show-password-input';
import { Item, Input, Icon, Label } from 'native-base';
import { HelperText, TextInput } from 'react-native-paper';

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      checkUserAccount: false,
      invalidEmail: false,
      invalidPass: false,
      userEmail: "",
      userPass: '',
      confPass: '',
      diffPass: false,
      icon: "eye-off",
      password: true,
      text: '',
      showPass: true
    }
    this.emailSend = this.emailSend.bind(this);
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  emailSend(text) {
    console.log(text);
    if(this.validateEmail(text)) {
      this.setState({
        invalidEmail: false,
        userEmail: text
      });
    } else {
      this.setState({
        invalidEmail: true,
        userEmail: text
      });
    }
  }

  resetPass() {
    //reset pass
    if(this.validateEmail(this.state.userEmail)) {
        this.setState({
            invalidEmail: false,
        });
        this.props.navigation.navigate('Signin');
    } else {
        
        this.setState({
            invalidEmail: true
        });
    }
    
  }

  renderForm() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View style={{marginTop: '40%'}}>
          <View style={{width: '100%', textAlign: 'left'}}>
            <Text style={{fontSize: 35, fontWeight: 'bold'}}>{data.Login.ForgotPassword}?</Text>
          </View>
          <View style={{marginTop: '5%'}}>
            <TextInput
              label={data.Login.email}
              value={this.state.userEmail}
              style={{backgroundColor: 'white'}}
              error={this.state.invalidEmail}
              onChangeText={(text) => this.emailSend(text)}
            />
          </View>
          <View style={styles.widthFull}>
            <TouchableOpacity onPress={() => this.resetPass()} style={styles.sendButton}><Text style={styles.colorWhite}>{data.Login.ResetPassword}</Text></TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }

  renderActivityInd() {
    return (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#00ff00" />
      </View>
    );
  }

  render() {
    return (
      !this.state.checkUserAccount ? 
        this.renderForm() : 
        this.renderActivityInd()

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: '5%'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'silver'
  },
  textInputField: {
    width: '100%',
    fontSize: 13,
    height: 30,
    // borderColor: 'silver', 
    // borderWidth: 2
    backgroundColor: 'white',
  },
  widthContainer: {
    width: '100%',
    alignItems: 'flex-start',
    textAlign: 'left',
  },
  flexDecoretion: {
    flexDirection: 'row'
  },
  importantField: {
    marginLeft: 10,
    color: 'red',
    fontSize: 30
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  sendButton: {
    height: 50,
    width: '100%',
    backgroundColor: '#6ec1e3',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 5
  },
  emailInvalid: {
    color: 'red',
    fontSize: 13
  },
  invalidForm: {
    borderColor: 'red'
  },
  mandatory: {
    width: '100%',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  colorRed: {
    color: 'red'
  },
  widthFull: {
    width: '100%',
    marginTop: 50
  },
  widthFullLess: {
    width: '100%',
    marginTop: 30
  },
  colorWhite: {
    color: 'white',
    fontSize: 20
  },
  textDecoration: {
    textDecorationLine: "underline",
    textDecorationStyle: "solid",
    textDecorationColor: "#000"
  },
  pass: {
    width: 50,
    height: 50,
    backgroundColor: 'blue'
  }
});
