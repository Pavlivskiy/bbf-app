import React, { Component } from 'react';
import { Button,Container,Header,Left,Right,Icon,Text } from 'native-base';
import {
  Platform, 
  StyleSheet, 
} from 'react-native';
import { createAppContainer, createStackNavigator, StackActions, NavigationActions, HeaderBackButton } from 'react-navigation'; // Version can be specified in package.json
import Signin from './Signin';
import Signup from './Signup';
import LoginFirstPage from './LoginFirstPage';
import { Settings } from '../Settings/Setting';
import ForgotPassword from './ForgotPassword';
import Congratulation from './Congratulation';

const myProps = this.props;

const AppNavigator = createStackNavigator({
    Home: {
      screen: LoginFirstPage,
    },
    Signup: {
      screen: Signup,
      data: {myProps},
    },
    Signin: {
      screen: Signin,
      data: {myProps},
    },
    ForgotPassword: {
      screen: ForgotPassword
    },
    Congratulation: {
      screen: Congratulation
    }
  },
  {
    initialRouteName: "Home"
});

const App = createAppContainer(AppNavigator);


export default class Login extends Component {
  render() {
    return (
      // <Container>
      //       <Header style={styles.container}>
      //         <Left style={{ flexDirection: 'row'}}>
      //           <Icon onPress={() => this.props.navigation.openDrawer()} name="md-menu" style={{ color: 'white', marginRight: 15 }} />
      //         </Left>
      //       </Header>
      <App screenProps={{ rootNavigation: this.props.navigation }} />
      // </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: '#6ec1e3',
    // padding: '5%'
  },
});