import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View,
    ActivityIndicator,
    TouchableOpacity,
    AsyncStorage,
    Image
} from 'react-native';
import * as data from '../../languages/en.json';
import Reinput from 'reinput';
import PasswordInputText from 'react-native-hide-show-password-input';
import { Item, Input, Icon, Label, Button, Container,Header,Left,Right  } from 'native-base';
import { HelperText, TextInput } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class Signin extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      checkUserAccount: false,
      invalidEmail: false,
      invalidPass: false,
      invalidName: false,
      userEmail: "",
      userPass: '',
      confPass: '',
      diffPass: false,
      icon: "eye-off",
      iconConfirm: "eye-off",
      showPass: true,
      showPassConfirm: true,
      disableButton: true,
      checkVal: true
    }
    this.checkPass = this.checkPass.bind(this);
    this.emailSend = this.emailSend.bind(this);
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  emailSend(text) {
    console.log(text);
    if(this.validateEmail(text)) {
      this.setState({
        invalidEmail: false,
        userEmail: text
      });
    } else {
      this.setState({
        invalidEmail: true,
        userEmail: text
      });
    }
    this.unableButton();
  }

  checkPass(text) {
    if(text.length < 8) {
      this.setState({
        invalidPass: true,
        userPass: text
      });
    } else {
      this.setState({
        invalidPass: false,
        userPass: text
      });
    }
    this.unableButton();
  }

  confirmPassword(text) {
    if(this.state.userPass !== "") {
      if(this.state.userPass !== text) {
        this.setState({
          diffPass: true,
          confPass: text
        });
      } else {
        this.setState({
          diffPass: false,
          confPass: text
        });
      } 
    } else {
      this.setState({
        invalidPass: true,
        diffPass: true,
        confPass: text
      });
    }
    this.unableButton();
  }

  validationForm = async () => {
    let { state } = this.state;
    if(this.state.disableButton === true) {
      if(this.state.userPass === "" || this.state.userPass === " ") {
        this.setState({
          invalidPass: true
        });
      } else if(this.state.userEmail === "" || this.state.userEmail === " ") {
        this.setState({
          invalidEmail: true
        });
      } else {
        console.log(this.state.userName);
        let usersData = {
          userName: this.state.userName,
          userEmail: this.state.userEmail,
          userPass: this.state.userPass
        }
  
        let usersDataString = usersData.toString();
        this.props.navigation.navigate('Congratulation');
        try {
          await AsyncStorage.setItem('UsersData', usersDataString);
        } catch (error) {
          // Error saving data
          console.log(error);
        }
      }
    } 
  }

  _changeIconPass() {
    console.log(this.state.showPass);
    this.setState(prevState => ({
        icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
        showPass: !prevState.showPass
    }));
    this.unableButton();
  }

  _changeIconConfirm() {
    console.log(this.state.showPassConfirm);
    this.setState(prevState => ({
        iconConfirm: prevState.iconConfirm === 'eye' ? 'eye-off' : 'eye',
        showPassConfirm: !prevState.showPassConfirm
    }));
    this.unableButton();
  }

  checkName(text) {
    if(text.length > 2) {
      this.setState({
        userName: text,
        invalidName: false
      })
    } else {
      this.setState({
        userName: text,
        invalidName: true
      })
    }
    this.unableButton();
  }
  unableButton() {
    if(this.state.invalidName === true) {
      this.setState({
        disableButton: true,
        checkVal: false
      });
    } 
    if(this.state.userPass === "" || this.state.userPass === " ") {
      this.setState({
        disableButton: true,
        checkVal: false
      });
    } 
    if(this.state.userEmail === "" || this.state.userEmail === " ") {
      this.setState({
        disableButton: true,
        checkVal: false
      });
    } 
    if(this.state.userPass !== this.state.confPass) {
      this.setState({
        disableButton: true,
        checkVal: false
      });
    } 
    if(this.state.checkVal) {
      this.setState({
        disableButton: false
      });
    }
  }
  renderForm() {
    return (
      <Container>
            <Header style={styles.containerHeader}>
              <Left style={[{ flexDirection: 'row'}]}>
                <Icon onPress={() => this.props.navigation.navigate('Home')} name="ios-arrow-round-back" style={{ color: 'black', marginLeft: '10%',fontSize: 40 }} />
              </Left>
              <Right>
              </Right>
            </Header>
      <KeyboardAwareScrollView style={styles.container}>
        <View style={ Platform.OS === 'ios' ? {marginTop: '20%'} : {marginTop: '5%'}}>
          <View style={{width: '100%', textAlign: 'left'}}>
            <Text style={{fontSize: 35, fontWeight: 'bold'}}>{data.Login.register} </Text>
          </View>
          <View style={{marginTop: '10%', flexDirection: 'row'}}>
            <View style={{width: '95%'}}>
              <TextInput
                label={data.Login.UserName}
                value={this.state.userName}
                style={{backgroundColor: 'white'}}
                onChangeText={(text) => this.checkName(text)}
                maxLength = {32}
                minLength={2}
              />
            </View>
          </View>
          {this.state.invalidName ? <Text style={styles.emailInvalid}>{data.Login.invalidName}</Text> : null}
          <View style={{marginTop: '10%', flexDirection: 'row'}}>
            <View style={{width: '95%'}}>
              <TextInput
                label={data.Login.email}
                value={this.state.userEmail}
                style={{backgroundColor: 'white'}}
                error={this.state.invalidEmail}
                onChangeText={(text) => this.emailSend(text)}
              />
            </View>
            <View style={{textAlign: 'left', alignItems: 'center', justifyContent: 'flex-end'}}><Text style={{fontSize: 25, color: 'red'}}>*</Text></View>
          </View>
          
          <View style={{flexDirection: 'row', marginTop: '10%'}}>
            <View style={{width: '95%'}}>
              <TextInput
                label={data.Login.Password}
                value={this.state.userPass}
                style={{backgroundColor: 'white', width: '100%'}}
                secureTextEntry={this.state.showPass}
                error={this.state.invalidPass}
                onChangeText={(text) => this.checkPass(text)}
              />
            
              <Icon style={{position: 'absolute', top: 20, right: 10}} name={this.state.icon} onPress={() => this._changeIconPass()} />
              </View>
              <View style={{textAlign: 'left', alignItems: 'center', justifyContent: 'flex-end'}}><Text style={{fontSize: 25, color: 'red'}}>*</Text></View>
          </View>
          {this.state.invalidPass ? <Text style={styles.emailInvalid}>{data.Login.invalidPassword}</Text> : null}
          <View style={{flexDirection: 'row', marginTop: '10%'}}>
            <View style={{width: '95%'}}>
              <TextInput
                label={data.Login.ConfPassw}
                value={this.state.confPass}
                style={{backgroundColor: 'white', width: '100%'}}
                secureTextEntry={this.state.showPassConfirm}
                error={this.state.diffPass}
                onChangeText={(text) => this.confirmPassword(text)}
              />
            </View>
              <Icon style={{position: 'absolute', top: 20, right: 25}} name={this.state.iconConfirm} onPress={() => this._changeIconConfirm()} />
              <View style={{textAlign: 'left', alignItems: 'center', justifyContent: 'flex-end'}}><Text style={{fontSize: 25, color: 'red'}}>*</Text></View>
          </View>
          {this.state.diffPass ? <Text style={styles.emailInvalid}>{data.Login.passDifferent}</Text> : null}
          <View style={{textAlign: 'left', flexDirection: 'row', marginTop: 20, marginLeft: '5%'}}><Text style={{fontSize: 25, color: 'red', marginTop: -1}}>*</Text><Text style={{color: 'red'}}> mandatory</Text></View>
          <View style={this.state.disableButton ? styles.disableButtonView : styles.widthFull}>
            <TouchableOpacity onPress={this.validationForm.bind(this)} style={this.state.disableButton ? styles.disableButtonTouch : styles.sendButton}><Text style={[this.state.disableButton ? styles.disableButton : styles.colorWhite]}>{data.Login.register}</Text></TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
      </Container>
    );
  }

  renderActivityInd() {
    return (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#00ff00" />
      </View>
    );
  }

  render() {
    return (
      !this.state.checkUserAccount ? 
        this.renderForm() : 
        this.renderActivityInd()

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: '5%'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'silver'
  },
  textInputField: {
    width: '90%',
    fontSize: 13,
    height: 30,
    borderColor: 'silver', 
    borderWidth: 2
  },
  widthContainer: {
    width: '100%',
    alignItems: 'flex-start',
    textAlign: 'left',
  },
  flexDecoretion: {
    flexDirection: 'row'
  },
  importantField: {
    marginLeft: 10,
    color: 'red',
    fontSize: 30
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  sendButton: {
    height: 50,
    width: '100%',
    backgroundColor: '#6ec1e3',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 5
  },
  disableButtonTouch: {
    height: 50,
    width: '100%',
    backgroundColor: 'silver',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 5
  },
  emailInvalid: {
    color: 'red',
    fontSize: 13
  },
  invalidForm: {
    borderColor: 'red'
  },
  mandatory: {
    width: '100%',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  colorRed: {
    color: 'red'
  },
  widthFull: {
    width: '100%',
    marginTop: '5%'
  },
  disableButtonView: {
    width: '100%',
    marginTop: '5%'
  },
  colorWhite: {
    color: 'white',
    fontSize: 20
  },
  disableButton: {
    color: 'gray',
    fontSize: 20
  },
  containerHeader: {
    backgroundColor: 'transparent',
    borderBottomWidth: 0,
},
});
