import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    View, 
    Image,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { Button,Container,Header,Left,Right,Icon,Text } from 'native-base';
import * as data from '../../languages/en.json';
import splash2 from '../../../assets/icon.png'
const {height} = Dimensions.get('window')

export default class LoginFirstPage extends Component {
    static navigationOptions = { title: data.Menu.login, header: null };
    componentDidMount() {
        console.log('ddddddddd');
        console.log(this.props);

    }
    render() {
        return (
            <Container>
            <Header style={styles.containerHeader}>
              <Left style={[{ flexDirection: 'row'}]}>
                <Icon onPress={() => this.props.screenProps.rootNavigation.navigate('Menu')} name="md-menu" style={{ color: 'white', marginLeft: '10%' }} />
              </Left>
              <Right>
                {/* <Icon name="md-search" style={{ color: 'white' }} /> */}
              </Right>
            </Header>
            <View style={styles.container}>
                <View style={{width: '100%', alignItems: 'flex-start'}}>
                    <Image style={styles.imageSplash} source={splash2} />
                    <Text style={styles.largeText}>{data.Login.startText}?</Text>
                </View>
                
                <View style={styles.bottomButton}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup')} style={[styles.buttonStyle, {backgroundColor: '#6ec1e3'}]}>
                        <Text style={{color: 'white'}}>Sign Up</Text>
                        </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Signin')} style={[styles.buttonStyle, {borderWidth: 2, borderColor: '#6ec1e3'}]}>
                        <Text style={{color: '#6ec1e3'}}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    containerHeader: {
        backgroundColor: '#6ec1e3',
    },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 20,
    marginTop: '20%'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  imageSplash: {
      height: 75,
      width: 75
  },
  largeText: {
      fontSize: 35,
      fontWeight: 'bold',
      marginTop: 50
  },
  bottomButton: {
      position: 'absolute',
      bottom: '10%',
      width: '100%',
  },
  buttonStyle: {
      height: 50,
      width: '100%',
      borderRadius: 5,
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 0.02 * height
  },

});
