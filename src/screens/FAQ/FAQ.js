import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View,
    ActivityIndicator,
    TouchableOpacity,
    Image,
    FlatList
} from 'react-native';
import { Button,Container,Header,Left,Right,Icon } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

const dataToSave = [
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.3,

    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 355.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 356.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 357.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.7,
        
    }
];
const dataToSaveFlop = [
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.1,

    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 355.2,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 356.3,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 357.4,
        
    },
    {
        title: 'Carrer de Can Campaner, 1, 007003 Palma, illes Balears, Spain',
        valueSensor: 354.5,
        
    }
];
const data = [
    "hello",
    'newOne'
]

export default class FAQ extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
    //this.getData();

  }

  render() {
    return (
      <Container>
        <View styles={styles.container}>
            <Header style={styles.containerHeader}>
                <Left style={{ flexDirection: 'row'}}>
                    <Icon size={30} onPress={() => this.props.navigation.navigate('Menu', {screenName: 'FAQ'})} name="ios-arrow-round-back" style={{ color: 'white', marginLeft: '10%', fontSize: 40}} />
                </Left>
                <Right>
                </Right>
            </Header>
          <View style={{backgroundColor: '#6ec1e3', flexDirection: 'row', height: '11%', justifyContent: 'center'}}>

          </View>
          
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: '5%'
  },
  containerHeader: {
    backgroundColor: '#6ec1e3',
    // borderColor: 'transparent',
    // borderBottomWidth: 0,
    // shadowColor: 'white'
  },
});
