
import Maps from '../screens/Map/Map';
import Settings from '../screens/Settings/Setting';
import Login from '../screens/Login/Login';
import { createStackNavigator,createAppContainer } from 'react-navigation'
import Menu from '../screens/Menu/Menu'
import Top from '../screens/Top/Top';
import Favorites from '../screens/Favorites/Favorites';
import OwnSensors from '../screens/OwnSensors/OwnSensors';
import FAQ from '../screens/FAQ/FAQ';

const MyDrawerNavigator = createStackNavigator({
  OwnSensors: {
    screen: OwnSensors
  },
  Map:{ 
    screen: Maps,
  },
  Favorites: {
    screen: Favorites
  },
  Top: {
    screen: Top
  },
  
  Login: {
    screen: Login
  },
  Settings: {
    screen: Settings
  },
  
  
  Menu: {
    screen: Menu
  },
  
 },
 {
  headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }
});
 
const MainContent = createAppContainer(MyDrawerNavigator);

export default MainContent;