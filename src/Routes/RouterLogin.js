import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import * as data from '../../languages/en.json';

export default class LoginFirstPage extends Component {
    static navigationOptions = { title: data.map, header: { visible:false } };
    render() {
        return (
        <View style={styles.container}>
            <Text style={styles.welcome}>LoginFirstPage</Text>
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
