import { AsyncStorage } from 'react-native';
import config from "../config";
import { apiService } from "../api/ApiService";

class DataService {
  constructor() {
    this.state = {
      stars: [],
      points: []
    };
  }

  async initStorage() {
    // AsyncStorage.clear();
    // const points = require('D:/MyProject/BitGeoPrototype/assets/changedData.json')

    const points = await apiService.getJSON('data/all').then((response) => {
      console.log('jjjjjjjjjjjjj');
      console.log(response);
      console.log(response.length);
      return response;
    });
    this.updateState({ 'points': points })
    const geo = points.map(item => (
      {
        ...item,
        location: {
          latitude: item.geometry.coordinates[1],
          longitude: item.geometry.coordinates[0],
        }
      }
    ));

    this.updateState({ 'geoPoints': geo });
    const stars = await this.getItemFromStorage('stars');
    if (stars) {
      this.updateState({ 'stars': JSON.parse(stars) });
    } else {
      this.updateState({ 'stars': [] });
    }
    console.log('======================');
    console.log('points: ' + stars);
    console.log('======================');
    // await this.loadFromStorage("language", config.defaultLanguage);
  }

  async setItemToStorage(key, value) {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
      console.log(error);
    }
  }

  async getItemFromStorage(key) {
    try {
      return await AsyncStorage.getItem(key);
      // console.log('itemFromStor '  + AsyncStorage.getItem(key));
    } catch (error) {
      console.log(error);
    }

  }

  updateState(updates) {
    // console.log("================================updates=======================")
    // console.log(updates);
    // console.log("================================updates=======================")    
    // console.log("================================state=======================")   
    // console.log(this.state);
    // console.log("================================state=======================")        
    this.state = Object.assign(this.state, updates);
  }

  getState() {
    console.log('console');
    console.log(this.state)
    return this.state;
  }

  async loadFromStorage(item, defaultValue) {
    let itemFromStorage = await AsyncStorage.getItem(item);
    this.state[item] = itemFromStorage ? itemFromStorage : defaultValue;
  }
}

//Singletons
let dataSource = new DataService();
export { dataSource };